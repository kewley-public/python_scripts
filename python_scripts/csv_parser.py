import csv
import re
from decimal import Decimal


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


column_converters = {
    'int': lambda x: int(x) if is_number(x) else 0,
    'float': lambda x: Decimal(x) if is_number(x) else Decimal(0.0),
    'string': lambda x: str(x),
    'currency': lambda x: column_sanitizers['currency'](x)
}

column_sanitizers = {
    'currency': lambda x: x.replace('$', '').replace(',', '').replace('(', '').replace(')', ''),
    'list': lambda x: list(map(lambda y: y.strip(), x.split(','))),
    'percent': lambda x: x.replace('%', ''),
    'default': lambda x: x
}

type_map = {
    'tripCalculationWindow': 'int',
    'trip_calculation_window': 'int',
    'reductionObserved': 'float',
    'reduction_observed': 'float',
    'hoursDown': 'int',
    'hours_down': 'int',
    'benefitHorizon': 'int',
    'benefit_horizon': 'int',
    'initialValueOfSubComponent': 'float',
    'initial_value_of_sub_component': 'float',
    'codLife': 'float',
    'cod_life': 'float',
    'maxLifeInMonths': 'int',
    'max_life_in_months': 'int',
    'curveSteepnessCs': 'int',
    'curve_steepness_cs': 'int',
    'degradationInflectionPoint': 'float',
    'degradation_inflection_point': 'float',
    'curveSteepnessCg': 'int',
    'curve_steepness_cg': 'int',
    'curveAccelerationFactor': 'int',
    'curve_acceleration_factor': 'int',
    'additionalLifeConsumptionThirtyDays': 'float',
    'additional_life_consumption_thirty_days': 'float',
    'additionalLifeConsumptionNinetyDays': 'float',
    'additional_life_consumption_ninety_days': 'float',
    'additionalLifeConsumptionOneYear': 'float',
    'additional_life_consumption_one_year': 'float',
    'proactiveRepairCost': 'float',
    'proactive_repair_cost': 'float',
    'maxCostOfComponentFailure': 'float',
    'max_cost_of_component_failure': 'float',
    'downTimeInDaysAccumulatedNotProactive': 'float',
    'down_time_in_days_accumulated_not_proactive': 'float',
    'downTimeInDaysAccumulatedProactive': 'float',
    'down_time_in_days_accumulated_proactive': 'float',
    'randomCatchRateProactiveWork': 'float',
    'random_catch_rate_proactive_work': 'float',
    'conversionRate': 'float',
    'conversion_rate': 'float',
    'consequentialDamageProbability': 'float',
    'consequential_damage_probability': 'float',
    'consequentialDamageCost': 'float',
    'consequential_damage_cost': 'float',
    'downstreamFailureProbability': 'float',
    'downstream_failure_probability': 'float',
    'downstreamFailureCost': 'float',
    'downstream_failure_cost': 'float',
    'unanticipatedAlrCost': 'float',
    'unanticipated_alr_cost': 'float',
    'assumedRefurbValueOfCodOfProactiveSubComponent': 'float',
    'assumed_refurb_value_of_cod_of_proactive_sub_component': 'float',
    'assumedProbabilityRefurbComponentCanBeUsed': 'float',
    'assumed_probability_refurb_component_can_be_used': 'float',
    'costToRestore': 'float',
    'cost_to_restore': 'float'
}


class CsvParser:
    def __init__(self):
        pass

    @staticmethod
    def read_items_from_csv(file_path, math_version=False):
        items = []
        with open(file_path, encoding="utf-8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            columns = None
            for row in csv_reader:
                if line_count == 0:
                    if math_version:
                        columns = list(map(CsvParser.convert, row))
                    else:
                        columns = row
                    line_count += 1
                else:
                    new_item = {}
                    row_count = 0
                    for c in columns:
                        if row[row_count] == '':
                            row_count += 1
                            continue
                        new_item[c] = CsvParser.sanitize_row_value(row[row_count], c)
                        row_count += 1
                    items.append(new_item)
                    line_count += 1
        return items

    @staticmethod
    def read_updates_from_csv(file_path, primary_key, column_key):
        updates = {}
        with open(file_path, encoding="utf-8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            columns = None
            for row in csv_reader:
                if line_count == 0:
                    columns = row
                else:
                    new_update = {}
                    row_count = 0
                    for c in columns:
                        if c == primary_key and row[row_count] != '':
                            new_update[primary_key] = row[row_count]
                        if c == column_key:
                            new_update[column_key] = CsvParser.sanitize_row_value(row[row_count], c)
                        row_count += 1
                    if primary_key in new_update and column_key in new_update:
                        primary_key_value = new_update[primary_key]
                        column_key_value = new_update[column_key]
                        if primary_key_value in updates and column_key == 'faultCodes':
                            updates[primary_key_value][column_key] = updates[primary_key_value][column_key] \
                                                                     + column_key_value
                        elif primary_key_value in updates:
                            updates[primary_key_value].update(new_update)
                        else:
                            updates[primary_key_value] = new_update
                line_count += 1
        return updates

    @staticmethod
    def convert(name):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    @staticmethod
    def sanitize_row_value(row_value, column_name):
        sanitized = row_value.strip()
        if column_name == 'faultCodes' or column_name == 'fault_codes':
            sanitized = column_sanitizers['list'](sanitized)
        else:
            sanitized = column_sanitizers['percent'](sanitized)
            sanitized = column_sanitizers['currency'](sanitized)

        if column_name in type_map:
            if type_map[column_name] == 'float':
                sanitized = column_converters['float'](sanitized)
            elif type_map[column_name] == 'int':
                sanitized = column_converters['int'](sanitized)

        return sanitized

    @staticmethod
    def strip_arg(x):
        return x.strip()
